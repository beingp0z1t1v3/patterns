package strategy;

import strategy.behaviours.fly.FlyBehavior;
import strategy.behaviours.fly.FlyWithWings;
import strategy.behaviours.fly.RadioFly;
import strategy.behaviours.fly.WithoutFly;
import strategy.behaviours.quack.LoudQuack;
import strategy.behaviours.quack.NoQuack;
import strategy.behaviours.quack.QuackBehavior;
import strategy.behaviours.quack.RareQuack;
import strategy.ducks.*;

public class Main {
    public static void main(String[] args) {

        Duck saxonduck = new SaxonDuck();
        FlyBehavior wingsfly = new FlyWithWings();
        QuackBehavior rarequack = new RareQuack();
        saxonduck.display();
        saxonduck.setFlyBehavior(wingsfly);
        saxonduck.setQuackBehavior(rarequack);
        saxonduck.performQuack();
        saxonduck.performFly();

        System.out.println("<========>");

        Duck rubberduck = new RubberDuck();
        FlyBehavior withoutfly = new WithoutFly();
        QuackBehavior noquack = new NoQuack();
        rubberduck.display();
        rubberduck.setFlyBehavior(withoutfly);
        rubberduck.setQuackBehavior(noquack);
        rubberduck.performQuack();
        rubberduck.performFly();

        System.out.println("<========>");

        Duck decoyduck = new DecoyDuck();
        QuackBehavior loudquack = new LoudQuack();
        FlyBehavior radiofly = new RadioFly();
        decoyduck.display();
        decoyduck.setFlyBehavior(withoutfly);
        decoyduck.setQuackBehavior(loudquack);
        decoyduck.performQuack();
        decoyduck.performFly();
        decoyduck.setFlyBehavior(radiofly);
        decoyduck.performFly();

        System.out.println("<========>");

        Duck redheadduck = new RedheadDuck();
        redheadduck.display();
        redheadduck.setQuackBehavior(noquack);
        redheadduck.performQuack();
        redheadduck.setQuackBehavior(rarequack);
        redheadduck.performQuack();
        redheadduck.setFlyBehavior(wingsfly);
        redheadduck.performFly();

    }
}
