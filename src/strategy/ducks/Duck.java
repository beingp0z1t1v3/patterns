package strategy.ducks;

import strategy.behaviours.fly.FlyBehavior;
import strategy.behaviours.quack.QuackBehavior;

public abstract class Duck {

    FlyBehavior flyBehavior;
    QuackBehavior quackBehavior;

    public Duck() {}

    public void setFlyBehavior (FlyBehavior fb) {
        this.flyBehavior = fb;
    }
    public void setQuackBehavior (QuackBehavior qb) {
        this.quackBehavior = qb;
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    public void performFly() {
        flyBehavior.fly();
    }

    public abstract void display();

    public void swim() {
        System.out.println("All strategy.ducks can swim");
    };

}
