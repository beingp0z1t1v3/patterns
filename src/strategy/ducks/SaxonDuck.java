package strategy.ducks;

public class SaxonDuck extends Duck {
    @Override
    public void display() {
        System.out.println("Я саксонская утка");
    }
}
