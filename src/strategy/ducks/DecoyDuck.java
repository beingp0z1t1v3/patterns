package strategy.ducks;

public class DecoyDuck extends Duck {
    @Override
    public void display() {
        System.out.println("Я утка-приманка");
    }
}
