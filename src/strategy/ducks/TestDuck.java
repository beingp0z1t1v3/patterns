package strategy.ducks;

public class TestDuck extends Duck {

    @Override
    public void display() {
        System.out.println("I'm a test duck!");
    }

}
