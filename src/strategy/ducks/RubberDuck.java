package strategy.ducks;

public class RubberDuck extends Duck {
    @Override
    public void display() {
        System.out.println("Я резиновая утка");
    }
}
