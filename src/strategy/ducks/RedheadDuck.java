package strategy.ducks;

public class RedheadDuck extends Duck {
    @Override
    public void display() {
        System.out.println("Я красноголовая утка");
    }
}
