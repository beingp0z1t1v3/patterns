package strategy.behaviours.fly;

public interface FlyBehavior {

    public void fly();

}
