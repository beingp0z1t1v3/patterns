package strategy.behaviours.fly;

public class WithoutFly implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("Я не летаю ");
    }
}
