package strategy.behaviours.fly;

public class RocketFly implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("Я летаю на ранце !");
    }
}
