package strategy.behaviours.fly;

public class RadioFly implements FlyBehavior{
    @Override
    public void fly() {
        System.out.println("Я летаю на радиоуправлении");
    }
}
