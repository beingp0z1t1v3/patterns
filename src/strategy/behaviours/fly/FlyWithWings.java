package strategy.behaviours.fly;

public class FlyWithWings implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("Я летаю на крыльях");
    }
}
