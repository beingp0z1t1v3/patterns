package strategy.behaviours.quack;

public class QuietQuack implements QuackBehavior{
    public void quack(){
        System.out.println("Я квакаю тихо");
    }
}
