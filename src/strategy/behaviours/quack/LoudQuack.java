package strategy.behaviours.quack;
public class LoudQuack implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("Я громко квакаю");
    }
}
