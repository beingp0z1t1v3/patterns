package strategy.behaviours.quack;

public class NoQuack implements QuackBehavior{
    @Override
    public void quack() {
        System.out.println("Я не квакаю");
    }
}
