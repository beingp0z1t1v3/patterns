package strategy.behaviours.quack;

public class RareQuack implements QuackBehavior{
    @Override
    public void quack() {
        System.out.println("Я редко квакаю");
    }
}
