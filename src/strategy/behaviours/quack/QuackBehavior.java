package strategy.behaviours.quack;

public interface QuackBehavior {

    public void quack();

}
